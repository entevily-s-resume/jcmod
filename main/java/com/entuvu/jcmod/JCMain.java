//http://bedrockminer.jimdo.com/modding-tutorials/basic-modding-1-8/

package com.entuvu.jcmod;

import com.entuvu.jcmod.core.proxy.CommonProxy;
import com.entuvu.jcmod.core.proxy.ServerProxy;
import com.entuvu.jcmod.core.tabs.JCCreativeTab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = JCReferences.MODID, name=JCReferences.NAME, version = JCReferences.VERSION)
public class JCMain
{
	@Instance(value = JCReferences.MODID) //Tell Forge what instance to use.
    public static JCMain instance;
	
	@SidedProxy(clientSide="com.entuvu.jcmod.core.proxy.ClientProxy", serverSide="com.entuvu.jcmod.core.proxy.ServerProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
	    this.proxy.preInit(e);
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
	    this.proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
	    this.proxy.postInit(e);
	}
	
	public static CreativeTabs tabStandard = new JCCreativeTab(CreativeTabs.getNextID(), JCReferences.MODID + "_tab");
}
