package com.entuvu.jcmod.core.client.render;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.entity.projectile.EntityRock;

import net.minecraftforge.fml.common.registry.EntityRegistry;

public class RenderEntity {
	public static void preInit() {
		EntityRegistry.registerModEntity(EntityRock.class, "Throwing Rock", 1, JCMain.instance, 64, 10, true);
	}
}
