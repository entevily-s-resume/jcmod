package com.entuvu.jcmod.core.blocks;

import java.util.Iterator;
import java.util.Random;

import com.entuvu.jcmod.JCMain;

import net.minecraft.block.Block;
import net.minecraft.block.BlockCactus;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.BlockFarmland;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;

public class BlockTopsoil extends BlockFarmland {
	
	String unlocalizedName;
	public BlockTopsoil(String unlocalizedName) {
		super();
		this.setUnlocalizedName(unlocalizedName);
		this.unlocalizedName = unlocalizedName;
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(0.6f);
		this.setHarvestLevel("shovel", 0);
	}
	
	@Override
	public boolean canSustainPlant(IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable) {
		/* We're currently saying all plants can be planted on this, event cacti */
		return true;
	}
	
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
		int i = ((Integer)state.getValue(MOISTURE)).intValue();

        if (!this.hasWater(worldIn, pos) && !worldIn.canLightningStrike(pos.up()))
        {
            if (i > 0)
            {
                worldIn.setBlockState(pos, state.withProperty(MOISTURE, Integer.valueOf(i - 1)), 2);
            }
            else if (!this.hasCrops(worldIn, pos))
            {
                worldIn.setBlockState(pos, Blocks.farmland.getStateFromMeta(i));
            }
        }
        else if (i < 7)
        {
            worldIn.setBlockState(pos, state.withProperty(MOISTURE, Integer.valueOf(7)), 2);
        }
        
        Block above = worldIn.getBlockState(pos.up()).getBlock();
        if(above instanceof BlockCrops){
        	if(((BlockCrops)above).canGrow(worldIn, pos.up(), worldIn.getBlockState(pos.up()), true)){
        		((BlockCrops)above).grow(worldIn, pos.up(), worldIn.getBlockState(pos.up()));
        	}
        }
	}
	
	private boolean hasWater(World worldIn, BlockPos pos)
    {
        Iterator iterator = BlockPos.getAllInBoxMutable(pos.add(-4, 0, -4), pos.add(4, 1, 4)).iterator();
        BlockPos.MutableBlockPos mutableblockpos;

        do
        {
            if (!iterator.hasNext())
            {
                return false;
            }

            mutableblockpos = (BlockPos.MutableBlockPos)iterator.next();
        }
        while (worldIn.getBlockState(mutableblockpos).getBlock().getMaterial() != Material.water);

        return true;
    }
	
	private boolean hasCrops(World worldIn, BlockPos pos)
    {
        Block block = worldIn.getBlockState(pos.up()).getBlock();
        return block instanceof net.minecraftforge.common.IPlantable && canSustainPlant(worldIn, pos, net.minecraft.util.EnumFacing.UP, (net.minecraftforge.common.IPlantable)block);
    }
	
	@Override
	public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock) {
		//super.onNeighborBlockChange(worldIn, pos, state, neighborBlock);

        if (worldIn.getBlockState(pos.up()).getBlock().getMaterial().isSolid() && !(worldIn.getBlockState(pos.up()).getBlock() instanceof BlockCactus))
        {
            worldIn.setBlockState(pos, Blocks.dirt.getDefaultState());
        }
	}	
}
