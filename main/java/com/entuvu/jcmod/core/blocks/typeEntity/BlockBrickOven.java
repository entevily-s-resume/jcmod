package com.entuvu.jcmod.core.blocks.typeEntity;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.builders.BlockWithFacing;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;

public class BlockBrickOven extends BlockWithFacing {
	public BlockBrickOven(String unlocalizedName) {
		super(Material.wood);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(2.5f);
		this.setHarvestLevel("axe", 1);
	}
	
	@Override
	public int getRenderType() {
		return 3;
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
    public boolean isFullCube() {
        return false;
    }
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return Item.getItemFromBlock(state.getBlock());
	}
}
