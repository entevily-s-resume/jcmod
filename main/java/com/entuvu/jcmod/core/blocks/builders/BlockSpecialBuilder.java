package com.entuvu.jcmod.core.blocks.builders;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.event.DropSpecial;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.storage.WorldInfo;

public class BlockSpecialBuilder extends Block {
	int blockID;
	
	public BlockSpecialBuilder(String unlocalizedName, int blockID) {
		super(SpecialBlocks.getMaterial(blockID));
		this.blockID = blockID;
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(SpecialBlocks.getHardness(blockID));
		this.setHarvestLevel(SpecialBlocks.getTool(blockID), SpecialBlocks.getToolLevel(blockID));
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return null;
	}
	
	@Override
	public boolean removedByPlayer(World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
		boolean didWork = super.removedByPlayer(world, pos, player, willHarvest);
		if(!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops") && didWork && !player.capabilities.isCreativeMode){
			int fortune = EnchantmentHelper.getFortuneModifier(player);
			SpecialBlocks.Drop(this.blockID, world, pos, fortune);
		}
		return didWork;
	}
}
