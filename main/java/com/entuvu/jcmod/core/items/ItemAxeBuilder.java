package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.ItemAxe;

public class ItemAxeBuilder extends ItemAxe {
	public ItemAxeBuilder(String unlocalizedName, ToolMaterial material) {
		super(material);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
}
