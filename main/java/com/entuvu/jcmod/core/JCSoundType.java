package com.entuvu.jcmod.core;

import com.entuvu.jcmod.JCReferences;

import net.minecraft.block.Block;

public class JCSoundType extends Block.SoundType{

	public final String soundNameStep;
	public final String soundNameBreak;
	public final String soundNamePlace;
	
	public JCSoundType(String name, float volume, float frequency) {
		super(name, volume, frequency);
		this.soundNameStep = null;
		this.soundNameBreak = null;
		this.soundNamePlace = null;
	}
	
	public JCSoundType(String soundNameBreak, String soundNameStep, float volume, float frequency){
		super(soundNameStep, volume, frequency);
		this.soundNameStep = soundNameStep;
		this.soundNameBreak = soundNameBreak;
		this.soundNamePlace = null;
	}
	
	public JCSoundType(String soundNameBreak, String soundNameStep, String soundNamePlace, float volume, float frequency){
		super(soundNameStep, volume, frequency);
		this.soundNameStep= soundNameStep;
		this.soundNameBreak = soundNameBreak;
		this.soundNamePlace = soundNamePlace;
	}
	
	@Override
	public String getBreakSound() {
		if(soundNameBreak == null)
			return JCReferences.MODID + ":dig." + this.soundName;
		else
			return this.soundNameBreak;
	}
	
	@Override
	public String getStepSound() {
		if(soundNameStep == null)
			return JCReferences.MODID + ":step." + this.soundName;
		else
			return this.soundNameStep;
	}
	
	public String getSoundNamePlace() {
		if(soundNamePlace == null)
			return getBreakSound();
		else
			return this.soundNamePlace;
	}

}
