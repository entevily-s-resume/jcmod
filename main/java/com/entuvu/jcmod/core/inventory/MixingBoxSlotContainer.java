package com.entuvu.jcmod.core.inventory;

import com.entuvu.jcmod.core.testing.TileEntityTestMixer;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class MixingBoxSlotContainer extends Slot {

	public MixingBoxSlotContainer(IInventory inventoryIn, int index, int xPosition, int yPosition) {
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack) {
		return TileEntityTestMixer.isItemContainer(stack);
	}
	
}
