package com.entuvu.jcmod.core.network;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.tileentity.TileEntityCampFire;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketCampFire implements IMessage{
	
	BlockPos pos;
	
	int furnaceBurnTime;
	int currentItemBurnTime;
	int cookTime;
	int totalCookTime;
	
	public static class PacketHandler implements IMessageHandler<PacketCampFire, IMessage>{

		@Override
		public IMessage onMessage(PacketCampFire message, MessageContext ctx) {
			EntityPlayer player = JCMain.proxy.getPlayerFromMessageContext(ctx);
			TileEntityCampFire campfire = (TileEntityCampFire)player.worldObj.getTileEntity(message.pos);

			if(campfire != null){
				campfire.setField(0, message.furnaceBurnTime);
				campfire.setField(1, message.currentItemBurnTime);
				campfire.setField(2, message.cookTime);
				campfire.setField(3, message.totalCookTime);
			}
			return null;
		}
		
	}
	
	public PacketCampFire(){
		pos = new BlockPos(0,0,0);
		furnaceBurnTime = 0 ;
	}
	
	public PacketCampFire(BlockPos loc, int furnaceBurnTime, int currentItemBurnTime, int currentCookTime, int totalCookTime) {
		pos = loc;
		this.furnaceBurnTime = furnaceBurnTime;
		this.currentItemBurnTime = currentItemBurnTime;
		this.cookTime = currentCookTime;
		this.totalCookTime = totalCookTime;
	}
	

	@Override
	public void fromBytes(ByteBuf buf) {
		pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
		furnaceBurnTime = buf.readInt();
		currentItemBurnTime = buf.readInt();
		cookTime = buf.readInt();
		totalCookTime = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(pos.getX());
		buf.writeInt(pos.getY());
		buf.writeInt(pos.getZ());
		
		buf.writeInt(furnaceBurnTime);
		buf.writeInt(currentItemBurnTime);
		buf.writeInt(cookTime);
		buf.writeInt(totalCookTime);
	}

}
