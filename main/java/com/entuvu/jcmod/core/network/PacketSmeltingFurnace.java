package com.entuvu.jcmod.core.network;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.tileentity.TileEntitySmeltingFurnace;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketSmeltingFurnace implements IMessage{

	BlockPos pos;
	
	int furnaceBurnTime;
	int currentItemBurnTime;
	int currentCookTime;
	int totalCookTime;
	
	public static class PacketHandler implements IMessageHandler<PacketSmeltingFurnace, IMessage>{

		@Override
		public IMessage onMessage(PacketSmeltingFurnace message, MessageContext ctx) {
			EntityPlayer player = JCMain.proxy.getPlayerFromMessageContext(ctx);
			
			TileEntitySmeltingFurnace furnace = (TileEntitySmeltingFurnace)player.worldObj.getTileEntity(message.pos);
			
			if(furnace != null){
				furnace.setField(0, message.furnaceBurnTime);
				furnace.setField(1, message.currentItemBurnTime);
				furnace.setField(2, message.currentCookTime);
				furnace.setField(3, message.totalCookTime);
			}
			
			return null;
		}
	}
	
	public PacketSmeltingFurnace() {
		pos = new BlockPos(0,0,0);
		furnaceBurnTime = 0;
	}
	
	public PacketSmeltingFurnace(BlockPos loc, int furnaceBurnTime, int currentItemBurnTime, int currentCookTime, int totalCookTime) {
		pos = loc;
		this.furnaceBurnTime = furnaceBurnTime;
		this.currentItemBurnTime = currentItemBurnTime;
		this.currentCookTime = currentCookTime;
		this.totalCookTime = totalCookTime;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
		furnaceBurnTime = buf.readInt();
		currentItemBurnTime = buf.readInt();
		currentCookTime = buf.readInt();
		totalCookTime = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(pos.getX());
		buf.writeInt(pos.getY());
		buf.writeInt(pos.getZ());
		
		buf.writeInt(furnaceBurnTime);
		buf.writeInt(currentItemBurnTime);
		buf.writeInt(currentCookTime);
		buf.writeInt(totalCookTime);
	}

}
