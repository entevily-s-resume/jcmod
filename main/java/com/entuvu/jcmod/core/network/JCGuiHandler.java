package com.entuvu.jcmod.core.network;

import com.entuvu.jcmod.core.client.gui.GuiTileEntityAlloyFurnace;
import com.entuvu.jcmod.core.client.gui.GuiTileEntityCampFire;
import com.entuvu.jcmod.core.client.gui.GuiTileEntityCrate;
import com.entuvu.jcmod.core.client.gui.GuiTileEntityMixer;
import com.entuvu.jcmod.core.client.gui.GuiTileEntitySmeltingFurnace;
import com.entuvu.jcmod.core.client.gui.GuiTileEntityTestMixer;
import com.entuvu.jcmod.core.client.gui.GuiTileEntityThatchBasket;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityAlloyFurnace;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityCampFire;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityCrate;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityMixer;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntitySmeltingFurnace;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityTestMixer;
import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityThatchBasket;
import com.entuvu.jcmod.core.testing.TileEntityTestMixer;
import com.entuvu.jcmod.core.tileentity.TileEntityAlloyFurnace;
import com.entuvu.jcmod.core.tileentity.TileEntityCampFire;
import com.entuvu.jcmod.core.tileentity.TileEntityCrate;
import com.entuvu.jcmod.core.tileentity.TileEntitySmeltingFurnace;
import com.entuvu.jcmod.core.tileentity.TileEntityThatchBasket;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class JCGuiHandler implements IGuiHandler{
	
	public static final int TILE_ENTITY_CRATE_GUI = 0;
	public static final int TILE_ENTITY_CAMP_FIRE_GUI = 1;
	public static final int TILE_ENTITY_THATCH_BASKET_GUI = 2;
	public static final int TILE_ENTITY_ALLOY_FURNACE_GUI = 3;
	public static final int TILE_ENTITY_SMELTING_FURNACE_GUI = 4;
	public static final int TILE_ENTITY_MIXER_GUI = 5;
	public static final int TILE_ENTITY_TEST_MIXER = 100;
	
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if(ID == TILE_ENTITY_CRATE_GUI)
			return new ContainerTileEntityCrate(player.inventory, (TileEntityCrate)world.getTileEntity(new BlockPos(x,y,z)));
		else if(ID ==  TILE_ENTITY_CAMP_FIRE_GUI)
			return new ContainerTileEntityCampFire(player.inventory, (TileEntityCampFire)world.getTileEntity(new BlockPos(x,y,z)));
		else if(ID ==  TILE_ENTITY_THATCH_BASKET_GUI)
			return new ContainerTileEntityThatchBasket(player.inventory, (TileEntityThatchBasket)world.getTileEntity(new BlockPos(x,y,z)));
		else if(ID ==  TILE_ENTITY_ALLOY_FURNACE_GUI)
			return new ContainerTileEntityAlloyFurnace(player.inventory, (TileEntityAlloyFurnace)world.getTileEntity(new BlockPos(x,y,z)));
		else if(ID ==  TILE_ENTITY_SMELTING_FURNACE_GUI){
			return new ContainerTileEntitySmeltingFurnace(player.inventory, (TileEntitySmeltingFurnace)world.getTileEntity(new BlockPos(x,y,z)));}
		else if(ID ==  TILE_ENTITY_MIXER_GUI){
			return new ContainerTileEntityMixer(player.inventory, world, new BlockPos(x,y,z));
		}
		else if(ID ==  TILE_ENTITY_TEST_MIXER){
			return new ContainerTileEntityTestMixer(player.inventory, (TileEntityTestMixer)world.getTileEntity(new BlockPos(x,y,z)));
		}
		
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if(ID == TILE_ENTITY_CRATE_GUI){
			return new GuiTileEntityCrate( player.inventory, (TileEntityCrate)world.getTileEntity(new BlockPos(x,y,z)));
		}
		else if(ID ==  TILE_ENTITY_CAMP_FIRE_GUI){
			return new GuiTileEntityCampFire( player.inventory, (TileEntityCampFire)world.getTileEntity(new BlockPos(x,y,z)));
		}
		else if(ID ==  TILE_ENTITY_THATCH_BASKET_GUI){
			return new GuiTileEntityThatchBasket( player.inventory, (TileEntityThatchBasket)world.getTileEntity(new BlockPos(x,y,z)));
		}
		else if(ID ==  TILE_ENTITY_ALLOY_FURNACE_GUI){
			return new GuiTileEntityAlloyFurnace( player.inventory, (TileEntityAlloyFurnace)world.getTileEntity(new BlockPos(x,y,z)));
		}
		else if(ID ==  TILE_ENTITY_SMELTING_FURNACE_GUI){
			return new GuiTileEntitySmeltingFurnace( player.inventory, (TileEntitySmeltingFurnace)world.getTileEntity(new BlockPos(x,y,z)));
		}
		else if(ID ==  TILE_ENTITY_MIXER_GUI){
			return new GuiTileEntityMixer(player.inventory, world, new BlockPos(x,y,z));
		}else if(ID ==  TILE_ENTITY_TEST_MIXER){
			return new GuiTileEntityTestMixer( player.inventory, (TileEntityTestMixer)world.getTileEntity(new BlockPos(x,y,z)));
		}
		return null;
	}

}
