package com.entuvu.jcmod.core.guicontainer;

import com.entuvu.jcmod.core.tileentity.TileEntityCampFire;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTileEntityCampFire extends Container {
	/*
	 * SLOTS:
	 * Tile Entity 0-3 ........ 0  - 3  [ 1 = burn-able, 3 = utensil, 0 = food, 2 = output ]
	 * Player Inventory 9-35 .. 4  - 30
	 * Player Inventory 0-8 ... 31 - 39
	 */
	
	//private CampFireTileEntity te;
	private IInventory tileCampFire;

	public ContainerTileEntityCampFire(InventoryPlayer playerInv, IInventory campFireInventory) {
		this.tileCampFire = campFireInventory;
		
		this.addSlotToContainer(new Slot(tileCampFire, 1, 61, 58)); 	// Burn-able Slot
		this.addSlotToContainer(new Slot(tileCampFire, 3, 36, 39)); 	// Utensil slot
		this.addSlotToContainer(new Slot(tileCampFire, 0, 61, 19)); 	// Food slot
		this.addSlotToContainer(new Slot(tileCampFire, 2, 123, 39));  	// Output
		
		// Player Inventory 9-35 .. 4  - 30
	    for (int y = 0; y < 3; ++y) {
	        for (int x = 0; x < 9; ++x) {
	            this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
	        }
	    }

	    // Player Inventory 0-8 ... 31 - 39
	    for (int x = 0; x < 9; ++x) {
	        this.addSlotToContainer(new Slot(playerInv, x, 8 + x * 18, 142));
	    }
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.tileCampFire.isUseableByPlayer(playerIn);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
		ItemStack previous = null;
		Slot slot = (Slot) this.inventorySlots.get(fromSlot);
		
		if(slot != null && slot.getHasStack()){
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			// Custom Start
			if(fromSlot < 4){
				// From TE inventory to player inventory
				if(!this.mergeItemStack(current, 4, 40, true))
					return null;
			}else{
				// From Player inventory to TE Inventory
				if(!this.mergeItemStack(current, 0, 3, false))
					return null;
			}
			// Custom End
			if(current.stackSize == 0)
				slot.putStack((ItemStack)null);
			else
				slot.onSlotChanged();
			
			if(current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(playerIn, current);
		}
		
		return previous;
	}


}
