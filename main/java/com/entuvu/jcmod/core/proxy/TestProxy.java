package com.entuvu.jcmod.core.proxy;

import com.entuvu.jcmod.JCReferences;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

public class TestProxy {
	public static Block test_mixer;
	
	

	public static void ClientInit() {
		Render();
	}

	public static void CommonPreInit() {
		Register();
	}
	
	public static void CommonInit() {
		
	}

	private static void Register() {
		//GameRegistry.registerBlock(test_mixer = new BlockTestMixer("test_mixer"), "test_mixer");
	}

	private static void Render() {
		//render(test_mixer);
		
		
	}

	public static void render(Block block){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(Item.getItemFromBlock(block),0,new ModelResourceLocation(JCReferences.MODID + ":" + block.getUnlocalizedName().substring(5),"inventory"));
	}
	
	public static void render(Block block, int meta, String file) {
	    Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
	    .register(Item.getItemFromBlock(block), meta, new ModelResourceLocation(JCReferences.MODID + ":" + file, "inventory"));
	}
	
	private static void render(Item metaItem, int i, String string) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(metaItem, i, new ModelResourceLocation(JCReferences.MODID + ":" + string, "inventory"));
	}

	public static void render(Item item){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(item, 0, new ModelResourceLocation(JCReferences.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}

	

}
