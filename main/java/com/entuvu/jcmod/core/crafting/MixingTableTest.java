package com.entuvu.jcmod.core.crafting;

import java.util.ArrayList;
import java.util.List;

import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class MixingTableTest {
	private static final MixingTableTest instance = new MixingTableTest();
	public final List recipes = Lists.newArrayList();
	
	public static MixingTableTest getInstance()
    {
        return instance;
    }
	
	public MixingTableTest() {
		this.addRecipe(new ItemStack(RegisterItem.sack_fertilizer, 1),  // Output
				RegisterItem.burlap_sack,  // Container
			    RegisterItem.pile_dirt,    // Items In any order
				RegisterItem.pile_gravel,
				RegisterItem.pile_dirt,
				RegisterItem.pile_gravel);
		
		this.addRecipe(new ItemStack(RegisterItem.sack_fertilizer, 1),  // Output
				RegisterItem.burlap_sack,  // Container
			    RegisterItem.pile_dirt,    // Items In any order
				RegisterItem.pile_dirt,
				RegisterItem.pile_dirt,
				RegisterItem.pile_dirt);
	}

	private void addRecipe(ItemStack output, Item container, Object ... recipeComponents) {
		ArrayList arraylist = Lists.newArrayList();
        Object[] aobject = recipeComponents;
        int i = recipeComponents.length;

        for (int j = 0; j < i; ++j)
        {
            Object object1 = aobject[j];

            if (object1 instanceof ItemStack)
            {
                arraylist.add(((ItemStack)object1).copy());
            }
            else if (object1 instanceof Item)
            {
                arraylist.add(new ItemStack((Item)object1));
            }
            else
            {
                if (!(object1 instanceof Block))
                {
                    throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + object1.getClass().getName() + "!");
                }

                arraylist.add(new ItemStack((Block)object1));
            }
        }

        this.recipes.add(new ShapelessRecipesWithContainer(output, container, arraylist));
	}
}
