
package com.entuvu.jcmod.core.crafting;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.entuvu.jcmod.core.register.RegisterBlock;
import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStone;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

public class JCCrafting {
	public static void initCrafting(){
		removeCraftingTableRecipes();
		removeSmeltingRecipes();
		addShapedRecipes();			
		addShapelessRecipes();
		addSmeltingRecipes();
	}
	
	//Vanilla Furnace//
	private static void addSmeltingRecipes() {
		GameRegistry.addSmelting(RegisterItem.clay_ingot_mold, new ItemStack(RegisterItem.ingot_mold, 1), 0.5F); //Ingot Mold//
		GameRegistry.addSmelting(RegisterItem.clay_bar_mold, new ItemStack(RegisterItem.bar_mold, 1), 0.5F); //Bar Mold//	
		GameRegistry.addSmelting(RegisterItem.clay_nail_mold, new ItemStack(RegisterItem.nail_mold, 1), 0.5F); //Nail Mold//	
		GameRegistry.addSmelting(RegisterItem.clay_arrowhead_mold, new ItemStack(RegisterItem.arrowhead_mold, 1), 0.5F); //Arrowhead Mold//
		GameRegistry.addSmelting(RegisterItem.clay_hinge_mold, new ItemStack(RegisterItem.hinge_mold, 1), 0.5F); //Hinge Mold//	
		GameRegistry.addSmelting(Items.clay_ball, new ItemStack(Items.brick, 3), 0.5F); //Clay brick//		
		GameRegistry.addSmelting(RegisterItem.pile_sand, new ItemStack(RegisterItem.brick_sand, 3), 0.5F); //Sand brick//		
		GameRegistry.addSmelting(RegisterItem.pile_red_sand, new ItemStack(RegisterItem.brick_red_sand, 3), 0.5F); //Red Sand brick//			
		GameRegistry.addSmelting(RegisterItem.copper_ingot, new ItemStack(RegisterItem.copper_nugget, 3), 0.5F); //Copper Nugget //		
		GameRegistry.addSmelting(Items.gold_ingot, new ItemStack(RegisterItem.gold_nugget, 3), 0.5F); //Gold Nugget //		
		GameRegistry.addSmelting(Items.iron_ingot, new ItemStack(RegisterItem.iron_nugget, 3), 0.5F); //Iron Nugget //		
		GameRegistry.addSmelting(RegisterItem.tin_ingot, new ItemStack(RegisterItem.tin_nugget, 3), 0.5F); //Tin Nugget //		
		GameRegistry.addSmelting(RegisterBlock.cobblestone_andesite, new ItemStack(Blocks.stone, 1, 5), 0.5F); //Andesite Block //		
		GameRegistry.addSmelting(RegisterBlock.cobblestone_diorite, new ItemStack(Blocks.stone, 1, 3), 0.5F); //Diorite Block //		
		GameRegistry.addSmelting(RegisterBlock.cobblestone_granite, new ItemStack(Blocks.stone, 1, 1), 0.5F); //Granite Block //
	}
	
	// Shapeless Recipes //
	private static void addShapelessRecipes(){
		GameRegistry.addShapelessRecipe(
				new ItemStack(Items.stick, 2), 
				new ItemStack(RegisterItem.flint_knife.setContainerItem(RegisterItem.flint_knife), 1, OreDictionary.WILDCARD_VALUE), //Stick - using Flint Knife// 
				new ItemStack(RegisterItem.branch));
		
		GameRegistry.addShapelessRecipe(
				new ItemStack(Items.stick, 3), 
				new ItemStack(RegisterItem.copper_knife.setContainerItem(RegisterItem.copper_knife), 1, OreDictionary.WILDCARD_VALUE), //Stick - using Copper Knife// 
				new ItemStack(RegisterItem.branch));
		
		GameRegistry.addShapelessRecipe(
				new ItemStack(Items.stick, 4), 
				new ItemStack(RegisterItem.iron_knife.setContainerItem(RegisterItem.copper_knife), 1, OreDictionary.WILDCARD_VALUE), //Stick - using Iron Knife// 
				new ItemStack(RegisterItem.branch));
		
		GameRegistry.addShapelessRecipe(
				new ItemStack(RegisterItem.wood_handle), 
				new ItemStack(RegisterItem.flint_knife.setContainerItem(RegisterItem.flint_knife), 1, OreDictionary.WILDCARD_VALUE), //Wood Handle - using Flint Knife// 
				new ItemStack(Items.stick));
		
		GameRegistry.addShapelessRecipe(
				new ItemStack(RegisterItem.wood_handle), 
				new ItemStack(RegisterItem.copper_knife.setContainerItem(RegisterItem.copper_knife), 1, OreDictionary.WILDCARD_VALUE), //Wood Handle - using Copper Knife// 
				new ItemStack(Items.stick));
		
		GameRegistry.addShapelessRecipe(
				new ItemStack(RegisterItem.wood_handle), 
				new ItemStack(RegisterItem.iron_knife.setContainerItem(RegisterItem.copper_knife), 1, OreDictionary.WILDCARD_VALUE), //Wood Handle - using Iron Knife// 
				new ItemStack(Items.stick));
	}

	
	// Shaped Recipes //
	private static void addShapedRecipes(){		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.clay_ingot_mold), //Clay Ingot Mold//
				"   ",
				"###",
				"###",
				'#', new ItemStack(Items.clay_ball));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.clay_bar_mold), //Clay Bar Mold//
				"   ",
				"## ",
				"## ",
				'#', new ItemStack(Items.clay_ball));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.clay_nail_mold), //Clay Nail Mold//
				"   ",
				" # ",
				" # ",
				'#', new ItemStack(Items.clay_ball));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.clay_arrowhead_mold), //Clay Arrowhead Mold//
				" ##",
				" ##",
				"   ",
				'#', new ItemStack(Items.clay_ball));		
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.clay_hinge_mold), //Clay Hinge Mold//
				" ##",
				" ##",
				" ##",
				'#', new ItemStack(Items.clay_ball));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.copper_sheet), //Copper Sheet//
				"  H",
				"## ",
				"## ",
				'#', new ItemStack(RegisterItem.copper_ingot),
				'H', new ItemStack(RegisterItem.iron_hammer.setContainerItem(RegisterItem.iron_hammer), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_sheet), //Bronze Sheet//
				"  H",
				"## ",
				"## ",
				'#', new ItemStack(RegisterItem.bronze_ingot),
				'H', new ItemStack(RegisterItem.iron_hammer.setContainerItem(RegisterItem.iron_hammer), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.gold_sheet), //Gold Sheet//
				"  H",
				"## ",
				"## ",
				'#', new ItemStack(Items.gold_ingot),
				'H', new ItemStack(RegisterItem.iron_hammer.setContainerItem(RegisterItem.iron_hammer), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.tin_sheet), //Tin Sheet//
				"  H",
				"## ",
				"## ",
				'#', new ItemStack(RegisterItem.tin_ingot),
				'H', new ItemStack(RegisterItem.iron_hammer.setContainerItem(RegisterItem.iron_hammer), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.tin_can), //Tin Can//
				" H ",
				" # ",
				" # ",
				'#', new ItemStack(RegisterItem.tin_ingot),
				'H', new ItemStack(RegisterItem.iron_hammer.setContainerItem(RegisterItem.iron_hammer), 1, OreDictionary.WILDCARD_VALUE));
		
		
		// Materials Natural //
		GameRegistry.addRecipe(new ItemStack(RegisterItem.twine), //Twine//
				"# ",
				"# ",
				'#', new ItemStack(RegisterItem.thatch));		
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.flint_shards, 2), //Flint Shards//
				"F ",
				"R ",
				'F', new ItemStack (Items.flint),
				'R', new ItemStack(RegisterItem.rock));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.kindling), //Kindling//
				" X ",
				"XTX",
				'X', new ItemStack(Items.stick),
				'T', new ItemStack(RegisterItem.thatch));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sieve), //Wood Sieve//
				"TXT",
				"XXX",
				"TXT",
				'X', new ItemStack(Items.stick),
				'T', new ItemStack(RegisterItem.twine));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.mortar, 1), //Mortar//
				"XX",
				"YY",
				'X', new ItemStack(RegisterItem.pile_sand),
				'Y', new ItemStack(Items.clay_ball));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.mortar, 1), //Mortar//
				"XX",
				"YY",
				'X', new ItemStack(RegisterItem.pile_dirt),
				'Y', new ItemStack(Items.clay_ball));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.sand, 1), //Dirt Block//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_sand));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.gravel, 1), //Sand Block//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_gravel));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.sand, 1, 1), //Red Sand Block//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_red_sand));	
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dirt, 1), //Gravel Block//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_dirt));		
		
		/*GameRegistry.addRecipe(new ItemStack(JCBlocks.sod, 2), //Sod Block//
				"##",
				"XX",
				'X', new ItemStack(JCItems.pile_dirt),
				'#', new ItemStack(JCItems.thatch));*/
		
		GameRegistry.addRecipe(new ItemStack(Items.dye, 1, 15), //Bone Meal//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.bone_shards));
		
		
		// Tool & Weapon Components //
		GameRegistry.addRecipe(new ItemStack(RegisterItem.stone_axe_head), //Stone Axe Head//
				"## ",
				"#  ",
				"   ",
				'#', new ItemStack(RegisterItem.rock));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.stone_hoe_head), //Stone Hoe Head//
				"## ",
				"   ",
				"   ",
				'#', new ItemStack(RegisterItem.rock));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.stone_shovel_head), //Stone Shovel head//
				" ##",
				" ##",
				"   ",
				'#', new ItemStack(RegisterItem.rock));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.stone_pickaxe_head), //Stone Pick Head//
				" # ",
				"# #",
				"   ",
				'#', new ItemStack(RegisterItem.rock));
		
		/*GameRegistry.addRecipe(new ItemStack(JCItems.copper_axe_head), //Copper Axe head//
				"## ",
				"#  ",
				"   ",
				'#', new ItemStack(JCItems.copper_ingot));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_hoe_head), //Copper Hoe Head//
				"## ",
				"   ",
				"   ",
				'#', new ItemStack(JCItems.copper_ingot));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_shovel_head), //Copper Shovel Head//
				" ##",
				" ##",
				"   ",
				'#', new ItemStack(JCItems.copper_ingot));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_pick_head), //Copper Pick Head//
				" # ",
				"# #",
				'#', new ItemStack(JCItems.copper_ingot));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_sword_hilt), //Copper Sword Hilt//
				" # ",
				" ##",
				"#  ",
				'#', new ItemStack(JCItems.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_sword_blade), //Copper Sword Blade//
				"  #",
				" # ",
				"#  ",
				'#', new ItemStack(JCItems.copper_ingot));*/
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.copper_saw_blade), //Copper Saw Blade//
				"   ",
				"###",
				"   ",
				'#', RegisterItem.copper_ingot);
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_axe_head), //Bronze Axe head//
				"## ",
				"#  ",
				"   ",
				'#', new ItemStack(RegisterItem.bronze_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_hoe_head), //Bronze Hoe Head//
				"## ",
				"   ",
				"   ",
				'#', new ItemStack(RegisterItem.bronze_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_shovel_head), //Bronze Shovel Head//
				" ##",
				" ##",
				"   ",
				'#', new ItemStack(RegisterItem.bronze_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_pick_head), //Bronze Pick Head//
				" # ",
				"# #",
				"   ",
				'#', new ItemStack(RegisterItem.bronze_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_sword_hilt), //Bronze Sword Hilt//
				" X ",
				" #X",
				"#  ",
				'X', new ItemStack(RegisterItem.bronze_ingot),
				'#', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_sword_blade), //Bronze Sword Blade//
				"  #",
				" # ",
				"#  ",
				'#', new ItemStack(RegisterItem.bronze_ingot));
		
		/*GameRegistry.addRecipe(new ItemStack(JCItems.bronze_saw_blade), //Bronze Saw Blade//				
				"   ",
				"###",
				"   ",
				'#', new ItemStack(JCItems.bronze_ingot));*/
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_axe_head), //Iron Axe head//
				"## ",
				"#  ",
				"   ",
				'#', new ItemStack(Items.iron_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_hammer_head), //Iron Hammer Head//
				"###",
				"###",
				"   ",
				'#', new ItemStack(Items.iron_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_hoe_head), //Iron Hoe Head//
				"## ",
				"   ",
				"   ",
				'#', new ItemStack(Items.iron_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_shovel_head), //Iron Shovel Head//
				" ##",
				" ##",
				"   ",
				'#', new ItemStack(Items.iron_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_pick_head), //Iron Pick Head//
				" # ",
				"# #",
				"   ",
				'#', new ItemStack(Items.iron_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_sword_hilt), //Iron Sword Hilt//
				" X ",
				" #X",
				"#  ",
				'X', new ItemStack(Items.iron_ingot),
				'#', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_sword_blade), //Iron Sword Blade//
				"  #",
				" # ",
				"#  ",
				'#', new ItemStack(Items.iron_ingot));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_saw_blade), //Iron Saw Blade//				
		"   ",
		"###",
		"   ",
		'#', new ItemStack(Items.iron_ingot));
		
		
		// Tools //
		GameRegistry.addRecipe(new ItemStack(RegisterItem.firestarter), //Fire Starter//
				" F",
				"R ",
				'F', new ItemStack (RegisterItem.flint_shards),
				'R', new ItemStack(RegisterItem.rock));
		
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.flint_axe), //Flint Axe//
				"I#",
				" A",
				'#', new ItemStack(RegisterItem.twine), 
				'I', new ItemStack(RegisterItem.flint_shards), 
				'A', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(Items.stone_axe), //Stone Axe//
				" #X",
				" I#",
				"   ",
				'#', new ItemStack(RegisterItem.twine),
				'X', new ItemStack(RegisterItem.stone_axe_head),
				'I', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(Items.stone_hoe), //Stone Hoe//
				" #X",
				" I#",
				"   ",
				'#', new ItemStack(RegisterItem.twine),
				'X', new ItemStack(RegisterItem.stone_hoe_head),
				'I', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(Items.stone_shovel), //Stone Shovel//
				" #X",
				" I#",
				"   ",
				'#', new ItemStack(RegisterItem.twine),
				'X', new ItemStack(RegisterItem.stone_shovel_head),
				'I', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(Items.stone_pickaxe), //Stone Pick//
				" #X",
				" I#",
				"   ",
				'#', new ItemStack(RegisterItem.twine),
				'X', new ItemStack(RegisterItem.stone_pickaxe_head),
				'I', new ItemStack(RegisterItem.wood_handle));
		
		/*GameRegistry.addRecipe(new ItemStack(JCItems.copper_axe), //Copper Axe//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(JCItems.copper_axe_head),
				'O', new ItemStack(JCItems.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_hoe), //Copper Hoe//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(JCItems.copper_hoe_head),
				'O', new ItemStack(JCItems.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_shovel), //Copper Shovel//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(JCItems.copper_shovel_head),
				'O', new ItemStack(JCItems.wood_handle));		
		
		GameRegistry.addRecipe(new ItemStack(JCItems.copper_pick), //Copper Pick//
				"  X",
				" I ",
				"   ",
				'X', new ItemStack(JCItems.copper_pick_head),
				'I', new ItemStack(JCItems.wood_handle));*/		
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.copper_saw), //Copper Saw //
				" H ",
				" B ",
				"   ",
				'B', RegisterItem.copper_saw_blade,
				'H', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.copper_chisel), //Copper Chisel//
				" X ",
				"XX ",
				"  X",
				'X', RegisterItem.copper_ingot);
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_axe), //Bronze Axe//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.bronze_axe_head),
				'O', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_hoe), //Bronze Hoe//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.bronze_hoe_head),
				'O', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_shovel), //Bronze Shovel//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.bronze_shovel_head),
				'O', new ItemStack(RegisterItem.wood_handle));		
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_pick), //Bronze Pick//
				"  X",
				" I ",
				"   ",
				'X', new ItemStack(RegisterItem.bronze_pick_head),
				'I', new ItemStack(RegisterItem.wood_handle));		
		
		/*GameRegistry.addRecipe(new ItemStack(JCItems.bronze_saw), //Bronze Saw //
				" H ",
				" B ",
				"   ",
				'B', JCItems.bronze_saw_blade,
				'H', new ItemStack(JCItems.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(JCItems.bronze_chisel), //Bronze Chisel//
				" X ",
				"XX ",
				"  X",
				'X', JCItems.bronze_ingot);*/
		
		GameRegistry.addRecipe(new ItemStack(Items.iron_axe), //Iron Axe//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.iron_axe_head),
				'O', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_hammer), //Iron Hammer//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.iron_hammer_head),
				'O', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(Items.iron_hoe), //Iron Hoe//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.iron_hoe_head),
				'O', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(Items.iron_shovel), //Iron Shovel//
				"  X",
				" O ",
				"   ",
				'X', new ItemStack(RegisterItem.iron_shovel_head),
				'O', new ItemStack(RegisterItem.wood_handle));		
		
		GameRegistry.addRecipe(new ItemStack(Items.iron_pickaxe), //Iron Pick//
				"  X",
				" I ",
				"   ",
				'X', new ItemStack(RegisterItem.iron_pick_head),
				'I', new ItemStack(RegisterItem.wood_handle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_saw), //Iron Saw //
				" H ",
				" B ",
				"   ",
				'B', RegisterItem.iron_saw_blade,
				'H', new ItemStack(RegisterItem.wood_handle));

		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_chisel), //Iron Chisel//
				" X ",
				"XX ",
				"  X",
				'X', Items.iron_ingot);
		
		
		// Weapons //
		GameRegistry.addRecipe(new ItemStack(RegisterItem.flint_knife), //Flint Knife//
				"#T", 
				" B",
				'#', new ItemStack(RegisterItem.flint_shards),
				'T', new ItemStack(RegisterItem.twine),
				'B', new ItemStack(RegisterItem.branch));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.copper_knife), //Copper Knife//
				"   ",
				"#TB", 
				"   ",
				'#', new ItemStack(RegisterItem.copper_bar),
				'T', new ItemStack(RegisterItem.twine),
				'B', new ItemStack(RegisterItem.wood_handle)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.iron_knife), //Iron Knife//
				"   ",
				"#TB", 
				"   ",
				'#', new ItemStack(RegisterItem.iron_bar),
				'T', new ItemStack(RegisterItem.twine),
				'B', new ItemStack(RegisterItem.wood_handle)); 
				
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_spear), //Wood Spear//
				" TF",
				" ST",
				"S  ",
				'T', new ItemStack(RegisterItem.twine), 
				'F', new ItemStack(RegisterItem.flint_shards), 
				'S', new ItemStack(Items.stick));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.copper_spear), //Copper Spear//
				" TA",
				" ST",
				"S  ",
				'T', new ItemStack(RegisterItem.twine), 
				'A', new ItemStack(RegisterItem.copper_arrowhead), 
				'S', new ItemStack(Items.stick));
		
		/*GameRegistry.addRecipe(new ItemStack(JCItems.copper_sword), //Copper Sword//
				"  #",
				" I ",
				"   ",
				'#', new ItemStack(JCItems.copper_sword_blade), 
				'I', new ItemStack(JCItems.copper_sword_hilt));*/	
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.bronze_sword), //Bronze Sword//
				"  #",
				" I ",
				"   ",
				'#', new ItemStack(RegisterItem.bronze_sword_blade), 
				'I', new ItemStack(RegisterItem.bronze_sword_hilt));
		
		GameRegistry.addRecipe(new ItemStack(Items.iron_sword), //Iron Sword//
				"  #",
				" I ",
				"   ",
				'#', new ItemStack(RegisterItem.iron_sword_blade), 
				'I', new ItemStack(RegisterItem.iron_sword_hilt));		
		
		
		// Miscellaneous Items //
		GameRegistry.addRecipe(new ItemStack(Blocks.ladder, 3), //Wood Ladder using Twine//
				"S S",
				"TST",
				"S S",
				'S', new ItemStack(Items.stick),
				'T', new ItemStack(RegisterItem.twine));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.ladder, 3), //Wood Ladder using Jute Fiber//
				"S S",
				"JSJ",
				"S S",
				'S', new ItemStack(Items.stick),
				'J', new ItemStack(RegisterItem.jute));
		
		/*GameRegistry.addRecipe(new ItemStack(Blocks.wall_sign, 2), //Wood Sign - wall mounted?//
				"   ",
				" W ",
				" S ",
				'S', new ItemStack(Items.stick),
				'W', new ItemStack(JCItems.wood_sheet));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.standing_sign, 2), //Wood Sign - standing?//
				"   ",
				" W ",
				" S ",
				'S', new ItemStack(Items.stick),
				'W', new ItemStack(JCItems.wood_sheet));*/
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_pressure_plate), //Wooden Pressure Plate//
				"   ",
				" W ",
				" X ",
				'X', new ItemStack(RegisterItem.iron_nugget),
				'W', new ItemStack(RegisterItem.wood_sheet));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.trapdoor), //Wooden Trap Door//
				"   ",
				"XW ",
				"   ",
				'X', new ItemStack(RegisterItem.iron_hinge),
				'W', new ItemStack(RegisterItem.wood_sheet));
		
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_palisade_bottom), //Wood Palisade Bottom//
				"   ",
				"L  ",
				"L  ",
				'L', new ItemStack(Blocks.log, 0, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_palisade_top), //Wood Palisade Top//
				"  L",
				"  L",
				"   ",
				'L', new ItemStack(Blocks.log, 0, OreDictionary.WILDCARD_VALUE));
	
		 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone), //Wood Stone Block//
				"###",
				"#S#",
				"###",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_fancy), //Wood Stone Block - Fancy//
				" # ",
				"#S#",
				" # ",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_no_top), //Wood Stone Block - No Top//
				"# #",
				"#S#",
				"###",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_no_bottom), //Wood Stone Block - No Bottom//
				"###",
				"#S#",
				"# #",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_no_left), //Wood Stone Block - No Left//
				"###",
				" S#",
				"###",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_no_right), //Wood Stone Block - No Right//
				"###",
				"#S ",
				"###",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));		
		
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_r), //Wood Stone Block - Right Diagonal//
				"SS#",
				"S#S",
				"#SS",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_l), //Wood Stone Block - Left Diagonal//
				"#SS",
				"S#S",
				"SS#",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_h), //Wood Stone Block - Horizontal//
				"SSS",
				"###",
				"SSS",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_stone_v), //Wood Stone Block - Vertical//
				"S#S",
				"S#S",
				"S#S",
				'S', new ItemStack(Blocks.stone),
				'#', new ItemStack(RegisterItem.plank_oak));
		
		/*GameRegistry.addRecipe(new ItemStack(JCBlocks.wood_stone), //Wall Log Left Block//
				"   ",
				"XX ",
				"XX ",
				'X', new ItemStack(Blocks.log, 0, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(JCBlocks.wood_stone), //Wall Log Middle Block//
				"   ",
				"XXX",
				"XXX",
				'X', new ItemStack(Blocks.log, 0, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(JCBlocks.wood_stone), //Wall Log Right Block//
				" XX",
				" XX",
				"   ",
				'X', new ItemStack(Blocks.log, 0, OreDictionary.WILDCARD_VALUE));*/
		
		GameRegistry.addRecipe(new ItemStack(Blocks.torch, 2), //Torch//
				"# ",
				"XF",
				'#', new ItemStack(RegisterItem.thatch),
				'X', new ItemStack(Items.stick),
				'F', new ItemStack(RegisterItem.firestarter.setContainerItem(RegisterItem.firestarter), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.thatch_basket), //Thatch Basket//
				"#X#",
				"#X#",
				"#X#",
				'#', new ItemStack(RegisterItem.thatch),
				'X', new ItemStack(Items.stick));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.thatch_basket), //Thatch Block//
				"###",
				"#T#",
				"###",
				'#', new ItemStack(RegisterItem.thatch),
				'T', new ItemStack(RegisterItem.twine));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.chest), //Chest - Oak Wood//
				"N N",
				"PPP",
				"###",
				'N', new ItemStack(RegisterItem.iron_hinge),
				'P', new ItemStack(RegisterItem.plank_oak),				
				'#', new ItemStack(Blocks.planks, 1, 0));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.chest), //Chest - Spruce Wood//
				"N N",
				"PPP",
				"###",
				'N', new ItemStack(RegisterItem.iron_hinge),
				'P', new ItemStack(RegisterItem.plank_spruce),				
				'#', new ItemStack(Blocks.planks, 1, 1));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.chest), //Chest - Birch Wood//
				"N N",
				"PPP",
				"###",
				'N', new ItemStack(RegisterItem.iron_hinge),
				'P', new ItemStack(RegisterItem.plank_birch),				
				'#', new ItemStack(Blocks.planks, 1, 2));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.chest), //Chest - Jungle Wood//
				"N N",
				"PPP",
				"###",
				'N', new ItemStack(RegisterItem.iron_hinge),
				'P', new ItemStack(RegisterItem.plank_jungle),				
				'#', new ItemStack(Blocks.planks, 1, 3));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.chest), //Chest - Acacia Wood//
				"N N",
				"PPP",
				"###",
				'N', new ItemStack(RegisterItem.iron_hinge),
				'P', new ItemStack(RegisterItem.plank_acacia),				
				'#', new ItemStack(Blocks.planks, 1, 4));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.chest), //Chest - Dark Oak Wood//
				"N N",
				"PPP",
				"###",
				'N', new ItemStack(RegisterItem.iron_hinge),
				'P', new ItemStack(RegisterItem.plank_darkoak),				
				'#', new ItemStack(Blocks.planks, 1, 5));
				
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.crate), //Wood Crate - Oak planks//
				"   ",
				"PP ",
				"PP ",
				'P', new ItemStack(RegisterItem.plank_oak)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.crate), //Wood Crate - Spruce planks//
				"   ",
				"PP ",
				"PP ",
				'P', new ItemStack(RegisterItem.plank_spruce)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.crate), //Wood Crate - Birch planks//
				"   ",
				"PP ",
				"PP ",
				'P', new ItemStack(RegisterItem.plank_birch)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.crate), //Wood Crate - Jungle planks//
				"   ",
				"PP ",
				"PP ",
				'P', new ItemStack(RegisterItem.plank_jungle)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.crate), //Wood Crate - Acacia planks//
				"   ",
				"PP ",
				"PP ",
				'P', new ItemStack(RegisterItem.plank_acacia)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.crate), //Wood Crate - Dark Oak planks//
				"   ",
				"PP ",
				"PP ",
				'P', new ItemStack(RegisterItem.plank_darkoak)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.camp_fire), //Camp Fire - Rocks//
				"RRR",
				"RKR",
				"RRR",
				'R', new ItemStack(RegisterItem.rock),
				'K', new ItemStack(RegisterItem.kindling));		
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.camp_fire), //Camp Fire - Andesite Rocks//
				"RRR",
				"RKR",
				"RRR",
				'R', new ItemStack(RegisterItem.rock_andesite),
				'K', new ItemStack(RegisterItem.kindling));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.camp_fire), //Camp Fire - Diorite Rocks//
				"RRR",
				"RKR",
				"RRR",
				'R', new ItemStack(RegisterItem.rock_diorite),
				'K', new ItemStack(RegisterItem.kindling)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.camp_fire), //Camp Fire - Granite Rocks//
				"RRR",
				"RKR",
				"RRR",
				'R', new ItemStack(RegisterItem.rock_granite),
				'K', new ItemStack(RegisterItem.kindling)); 

		/*GameRegistry.addRecipe(new ItemStack(JCBlocks.bigAssOven), 
				"I#I", 
				"# #", 
				"I#I", 
				'#', Items.iron_ingot, 
				'I', Blocks.log);	
		*/	
		
		GameRegistry.addRecipe(new ItemStack(Blocks.crafting_table), //Crafting Table - Oak Logs//
				"  ",
				"XX",
				'X', new ItemStack(Blocks.log, 0, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.crafting_table), //Crafting Table - Spruce Logs//
				"  ",
				"XX",
				'X', new ItemStack(Blocks.log, 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.crafting_table), //Crafting Table - Birch Logs//
				"  ",
				"XX",
				'X', new ItemStack(Blocks.log, 2, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.crafting_table), //Crafting Table - Jungle Logs//
				"  ",
				"XX",
				'X', new ItemStack(Blocks.log, 3, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.crafting_table), //Crafting Table - Acacia Logs//
				"  ",
				"XX",
				'X', new ItemStack(Blocks.log2, 0, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.crafting_table), //Crafting Table - Dark Oak Logs//
				"  ",
				"XX",
				'X', new ItemStack(Blocks.log2, 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.smelting_furnace), //Smelting Furnace//
				"###",
				"X X",
				"XXX",
				'X', new ItemStack(Blocks.cobblestone),
				'#', new ItemStack(Blocks.stone_slab, 1, 3));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.alloy_furnace), //Alloy Furnace//
				"###",
				"X X",
				"XXX",				
				'X', new ItemStack(Blocks.cobblestone),
				'#', new ItemStack(Blocks.stone_slab, 2, 0));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.brick_oven), //Brick Oven//
				"XXX",
				"X X",
				"XXX",				
				'X', new ItemStack(Items.brick));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.mixer), //Mixing Box - Oak planks//
				"   ",
				"X X",
				"XXX",				
				'X', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.mixer), //Mixing Box - Spruce planks//
				"   ",
				"X X",
				"XXX",				
				'X', new ItemStack(RegisterItem.plank_spruce));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.mixer), //Mixing Box - Birch planks//
				"   ",
				"X X",
				"XXX",				
				'X', new ItemStack(RegisterItem.plank_birch));	
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.mixer), //Mixing Box - Jungle planks//
				"   ",
				"X X",
				"XXX",				
				'X', new ItemStack(RegisterItem.plank_acacia));	
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.mixer), //Mixing Box - Acacia planks//
				"   ",
				"X X",
				"XXX",				
				'X', new ItemStack(RegisterItem.plank_jungle));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.mixer), //Mixing Box - Dark Oak planks//
				"   ",
				"X X",
				"XXX",				
				'X', new ItemStack(RegisterItem.plank_darkoak));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.burlap_sack), //Burlap Sack//
				"XX ",
				"XX ",
				"XX ",				
				'X', new ItemStack(RegisterItem.jute));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.can_grubs), //Can of grubs//
				"XXX",
				"X#X",
				"XXX",
				'X', new ItemStack(RegisterItem.grub),
				'#', new ItemStack(RegisterItem.tin_can));
		
		GameRegistry.addRecipe(new ItemStack(Items.fishing_rod), //Fishing Rod//
				"  S",
				" SX",
				"S #",
				'S', new ItemStack(Items.stick),
				'X', new ItemStack(Items.string),
				'#', new ItemStack(RegisterItem.can_grubs));
		
		
		// Material Rocks //
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.cobblestone_andesite), //Andesite Cobblestone//
				"XXX",
				"XMX",
				"XXX",
				'X', new ItemStack(RegisterItem.rock_andesite),
				'M', new ItemStack(RegisterItem.mortar));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.cobblestone_diorite), //Diorite Cobblestone//
				"XXX",
				"XMX",
				"XXX",
				'X', new ItemStack(RegisterItem.rock_diorite),
				'M', new ItemStack(RegisterItem.mortar));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.cobblestone_granite), //Granite Cobblestone//
				"XXX",
				"XMX",
				"XXX",
				'X', new ItemStack(RegisterItem.rock_granite),
				'M', new ItemStack(RegisterItem.mortar));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.cobblestone), //Cobblestone//
				"XXX",
				"XMX",
				"XXX",
				'X', new ItemStack(RegisterItem.rock),
				'M', new ItemStack(RegisterItem.mortar));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_stairs), //Cobblestone Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.cobblestone, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_stairs, 2), //Cobblestone Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.cobblestone, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.stairs_andesite), //Andesite Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 5),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.stairs_andesite, 2), //Andesite Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 5),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.stairs_diorite), //Diorite Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 3),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.stairs_diorite, 2), //Diorite Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 3),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.stairs_granite), //Granite Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 1),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.stairs_granite, 2), //Granite Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 1),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.slab_andesite), //Andesite Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 5),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.slab_andesite, 2), //Andesite Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 5),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.slab_diorite), //Diorite Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 3),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.slab_diorite, 2), //Diorite Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 3),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.slab_granite), //Granite Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 1),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.slab_granite, 2), //Granite Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 1),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.sandstone_stairs), //Sandstone Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.sandstone_stairs, 2), //Sandstone Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.red_sandstone_stairs), //Red Sandstone Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.red_sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.red_sandstone_stairs, 2), //Red Sandstone Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.red_sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.brick_stairs), //Brick Stairs using Copper Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.brick_block, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.brick_stairs, 2), //Brick Stairs using Iron Chisel//
				"C  ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.brick_block, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 2, 0), //Stone Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 4, 0), //Stone Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 2, 1), //Sandstone Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 4, 1), //Sandstone Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab2, 2, 0), //Red Sandstone Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.red_sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab2, 4, 0), //Red Sandstone Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.red_sandstone, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 1, 3), //Cobblestone Slab//
				"   ",
				"XMX",
				"XXX",
				'X', new ItemStack(RegisterItem.rock),
				'M', new ItemStack(RegisterItem.mortar)); 		
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 2, 4), //Brick Slab using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.brick_block, 1, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stone_slab, 4, 4), //Brick Slab using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.brick_block, 1, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.brick_block), //Brick Block//
				"BBB",
				"BXB",
				"BBB",
				'X', new ItemStack(RegisterItem.mortar),
				'B', new ItemStack(Items.brick));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.sandstone), //Sandstone Block//
				"BBB",
				"BXB",
				"BBB",
				'X', new ItemStack(RegisterItem.mortar),
				'B', new ItemStack(RegisterItem.brick_sand));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.red_sandstone), //Red Sandstone Block//
				"BBB",
				"BXB",
				"BBB",
				'X', new ItemStack(RegisterItem.mortar),
				'B', new ItemStack(RegisterItem.brick_red_sand));
		
		GameRegistry.addRecipe(new ItemStack(Items.brick), //Clay Brick//
				"CC ",
				"CC ",
				"CC ",
				'C', new ItemStack(RegisterItem.clay_shards));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.brick_stone, 2), //Stone Brick using Copper Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone_slab, 2, 0),
				'C', new ItemStack(RegisterItem.copper_chisel.setContainerItem(RegisterItem.copper_chisel), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.brick_stone, 4), //Stone Brick using Iron Chisel//
				" C ",
				" X ",
				"   ",
				'X', new ItemStack(Blocks.stone_slab, 2, 0),
				'C', new ItemStack(RegisterItem.iron_chisel.setContainerItem(RegisterItem.iron_chisel), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.stonebrick, 1, 0), //Stone Brick Block//
				"BBB",
				"BXB",
				"BBB",
				'X', new ItemStack(RegisterItem.mortar),
				'B', new ItemStack(RegisterItem.brick_stone));
		
		
		// Material Wood //
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_frame, 1), //Wood Frame Block - branch version using Twine//
				"BBB",
				"BXB",
				"BBB",
				'B', new ItemStack(RegisterItem.branch),
				'X', new ItemStack(RegisterItem.twine));
		
		GameRegistry.addRecipe(new ItemStack(RegisterBlock.wood_frame, 1), //Wood Frame Block - branch version using Jute Fiber//
				"BBB",
				"BXB",
				"BBB",
				'B', new ItemStack(RegisterItem.branch),
				'X', new ItemStack(RegisterItem.jute));		
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_oak, 2), //Oak Plank using Copper Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 0),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_oak, 4), //Oak Plank using Iron Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 0),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_spruce, 2), //Spruce Plank using Copper Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 1),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_spruce, 4), //Spruce Plank using Iron Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 1),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_birch, 2), //Birch Plank using Copper Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 2),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_birch, 4), //Birch Plank using Iron Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 2),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_jungle, 2), //Jungle Plank using Copper Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 3),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_jungle, 4), //Jungle Plank using Iron Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log, 1, 3),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_acacia, 2), //Acacia Plank using Copper Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log2, 1, 0),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_acacia, 4), //Acacia Plank using Iron Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log2, 1, 0),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_darkoak, 2), //Dark Oak Plank using Copper Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log2, 1, 1),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.plank_darkoak, 4), //Dark Oak Plank using Iron Saw//
				"   ",
				"SW ",
				"   ",
				'W', new ItemStack(Blocks.log2, 1, 1),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.planks, 1, 0), //Oak Plank Block//
				"WWW",
				"WXW",
				"WWW",
				'X', new ItemStack(RegisterItem.iron_nails),
				'W', new ItemStack(RegisterItem.plank_oak));
				
		GameRegistry.addRecipe(new ItemStack(Blocks.planks, 1, 1), //Spruce Plank Block//
				"WWW",
				"WXW",
				"WWW",
				'X', new ItemStack(RegisterItem.iron_nails),
				'W', new ItemStack(RegisterItem.plank_spruce));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.planks, 1, 2), //Birch Plank Block//
				"WWW",
				"WXW",
				"WWW",
				'X', new ItemStack(RegisterItem.iron_nails),
				'W', new ItemStack(RegisterItem.plank_birch));	
		
		GameRegistry.addRecipe(new ItemStack(Blocks.planks, 1, 3), //Jungle Plank Block//
				"WWW",
				"WXW",
				"WWW",
				'X', new ItemStack(RegisterItem.iron_nails),
				'W', new ItemStack(RegisterItem.plank_jungle));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.planks, 1, 4), //Acacia Plank Block//
				"WWW",
				"WXW",
				"WWW",
				'X', new ItemStack(RegisterItem.iron_nails),
				'W', new ItemStack(RegisterItem.plank_acacia));	
		
		GameRegistry.addRecipe(new ItemStack(Blocks.planks, 1, 5), //Dark Oak Plank Block//
				"WWW",
				"WXW",
				"WWW",
				'X', new ItemStack(RegisterItem.iron_nails),
				'W', new ItemStack(RegisterItem.plank_darkoak));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.oak_stairs), //Oak Stairs using Copper Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 0),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.oak_stairs, 2), //Oak Stairs using Iron Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 0),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.spruce_stairs), //Spruce Stairs using Copper Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 1),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.spruce_stairs, 2), //Spruce Stairs using Iron Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 1),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.birch_stairs), //Birch Stairs using Copper Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 2),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.birch_stairs, 2), //Birch Stairs using Iron Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 2),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.jungle_stairs), //Jungle Stairs using Copper Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 3),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.jungle_stairs, 2), //Jungle Stairs using Iron Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 3),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.acacia_stairs), //Acacia Stairs using Copper Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 4),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.acacia_stairs, 2), //Acacia Stairs using Iron Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 4),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dark_oak_stairs), //Dark Oak Stairs using Copper Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 5),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dark_oak_stairs, 2), //Dark Oak Stairs using Iron Saw//
				"S  ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 5),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 2, 0), //Oak Slab using Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 0),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 4, 0), //Oak Slab using Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 0),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 2, 1), //Spruce Slab using Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 1),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 4, 1), //Spruce Slab using Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 1),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 2, 2), //Birch Slab using Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 2),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 4, 2), //Birch Slab using Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 2),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 2, 3), //Jungle Slab using Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 3),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 4, 3), //Jungle Slab using Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 3),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 2, 4), //Acacia Slab using Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 4),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 4, 4), //Acacia Slab using Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 4),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 2, 5), //Dark Oak Slab using Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 5),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.wooden_slab, 4, 5), //Dark Oak Slab using Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.planks, 1, 5),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(Items.oak_door, 1), //Oak Door//
				"NPP",
				" PP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_oak),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Items.spruce_door, 1), //Spruce Door//
				"NPP",
				" PP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_spruce),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Items.birch_door, 1), //Birch Door//
				"NPP",
				" PP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_birch),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Items.jungle_door, 1), //Jungle Door//
				"NPP",
				" PP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_jungle),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Items.acacia_door, 1), //Acacia Door//
				"NPP",
				" PP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_acacia),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Items.dark_oak_door, 1), //Dark Oak Door//
				"NPP",
				" PP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_darkoak),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.oak_fence, 3), //Oak Fence//
				"   ",
				"#X#",
				"#X#",
				'#', new ItemStack(Blocks.log, 1, 0),
				'X', new ItemStack(RegisterItem.plank_oak));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.spruce_fence, 3), //Spruce Fence//
				"   ",
				"#X#",
				"#X#",
				'#', new ItemStack(Blocks.log, 1, 1),
				'X', new ItemStack(RegisterItem.plank_spruce));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.birch_fence, 3), //Birch Fence//
				"   ",
				"#X#",
				"#X#",
				'#', new ItemStack(Blocks.log, 1, 2),
				'X', new ItemStack(RegisterItem.plank_birch));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.jungle_fence, 3), //Jungle Fence//
				"   ",
				"#X#",
				"#X#",
				'#', new ItemStack(Blocks.log, 1, 3),
				'X', new ItemStack(RegisterItem.plank_jungle));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.acacia_fence, 3), //Acacia Fence//
				"   ",
				"#X#",
				"#X#",
				'#', new ItemStack(Blocks.log2, 1, 0),
				'X', new ItemStack(RegisterItem.plank_acacia));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dark_oak_fence, 3), //Dark Oak Fence//
				"   ",
				"#X#",
				"#X#",
				'#', new ItemStack(Blocks.log2, 1, 1),
				'X', new ItemStack(RegisterItem.plank_darkoak));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.oak_fence_gate, 1, 0), //Oak Fence Gate//
				"   ",
				"NPP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_oak),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.spruce_fence_gate, 1), //Spruce Fence Gate//
				"   ",
				"NPP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_spruce),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.birch_fence_gate, 1), //Birch Fence Gate//
				"   ",
				"NPP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_birch),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.jungle_fence_gate, 1), //Jungle Fence Gate//
				"   ",
				"NPP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_jungle),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(Blocks.acacia_fence_gate, 1), //Acacia Fence Gate//
				"   ",
				"NPP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_acacia),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dark_oak_fence_gate, 1), //Dark Oak Fence Gate//
				"   ",
				"NPP",
				"NPP",
				'P', new ItemStack(RegisterItem.plank_darkoak),
				'N', new ItemStack(RegisterItem.iron_hinge)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 2), //Wood Sheet using Oak Slab and Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 0),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 3), //Wood Sheet using Oak Slab and Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 0),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));  
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 2), //Wood Sheet using Spruce Slab and Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 1),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 3), //Wood Sheet using Spruce Slab and Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 1),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 2), //Wood Sheet using Birch Slab and Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 2),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 3), //Wood Sheet using Birch Slab and Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 2),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 2), //Wood Sheet using Jungle Slab and Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 3),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 3), //Wood Sheet using Jungle Slab and Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 3),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE));  
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 2), //Wood Sheet using Acacia Slab and Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 4),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 3), //Wood Sheet using Acacia Slab and Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 4),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 2), //Wood Sheet using Dark Oak Slab and Copper Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 5),
				'S', new ItemStack(RegisterItem.copper_saw.setContainerItem(RegisterItem.copper_saw), 1, OreDictionary.WILDCARD_VALUE));
		
		GameRegistry.addRecipe(new ItemStack(RegisterItem.wood_sheet, 3), //Wood Sheet using Dark Oak Slab and Iron Saw//
				" S ",
				" W ",
				"   ",
				'W', new ItemStack(Blocks.wooden_slab, 1, 5),
				'S', new ItemStack(RegisterItem.iron_saw.setContainerItem(RegisterItem.iron_saw), 1, OreDictionary.WILDCARD_VALUE)); 
		
		
		///////////////////////Temporary Recipes - Will eventually be moved to correct crafting stations/////////////////////		
		GameRegistry.addRecipe(new ItemStack(Blocks.sand, 1, 0), //Sand Block - Compactor//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_sand));	
		
		GameRegistry.addRecipe(new ItemStack(Blocks.sand, 1, 1), //Red Sand Block - Compactor//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_red_sand));	
		
		GameRegistry.addRecipe(new ItemStack(Blocks.gravel, 1), //Gravel Block - Compactor//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_gravel));
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dirt, 1), //Dirt Block - Compactor//
				"XX",
				"XX",
				'X', new ItemStack(RegisterItem.pile_dirt));		
	}

	private static void removeSmeltingRecipes() {
		/* Remove smelting recipes */
		removeSmeltingRecipe(Items.gold_ingot);
		removeSmeltingRecipe(Items.iron_ingot);
		removeSmeltingRecipe(Items.baked_potato);
		removeSmeltingRecipe(Items.cooked_beef);
		removeSmeltingRecipe(Items.cooked_chicken);
		removeSmeltingRecipe(Items.cooked_fish);
		removeSmeltingRecipe(Items.cooked_mutton);
		removeSmeltingRecipe(Items.cooked_porkchop);
		removeSmeltingRecipe(Items.cooked_rabbit);
		removeSmeltingRecipe(Items.mushroom_stew);
		removeSmeltingRecipe(Items.rabbit_stew);
	}

	private static void removeCraftingTableRecipes() {
		/* Remove Stairs */
		removeRecipe(Item.getItemFromBlock(Blocks.oak_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.spruce_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.birch_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.jungle_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.acacia_stairs));	
		removeRecipe(Item.getItemFromBlock(Blocks.dark_oak_stairs));		
		removeRecipe(Item.getItemFromBlock(Blocks.nether_brick_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.quartz_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.red_sandstone_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.sandstone_stairs));		
		removeRecipe(Item.getItemFromBlock(Blocks.stone_brick_stairs));
		removeRecipe(Item.getItemFromBlock(Blocks.stone_stairs));
		/* Remove Slabs */
		removeRecipe(Item.getItemFromBlock(Blocks.stone_slab));
		removeRecipe(Item.getItemFromBlock(Blocks.stone_slab2));
		removeRecipe(Item.getItemFromBlock(Blocks.wooden_slab));
		/* Remove Brick Blocks & Items */
		removeRecipe(Items.brick);
		removeRecipe(Item.getItemFromBlock(Blocks.brick_block));
		removeRecipe(Item.getItemFromBlock(Blocks.sandstone));
		removeRecipe(Item.getItemFromBlock(Blocks.red_sandstone));
		removeRecipe(Item.getItemFromBlock(Blocks.stonebrick));
		/* Remove Planks */
		removeRecipe(Item.getItemFromBlock(Blocks.planks));
		/* Remove Crafting Table */
		removeRecipe(Item.getItemFromBlock(Blocks.crafting_table));		
		/* Remove Wooden Doors */
		removeRecipe(Items.oak_door);
		removeRecipe(Items.spruce_door);
		removeRecipe(Items.birch_door);
		removeRecipe(Items.jungle_door);
		removeRecipe(Items.acacia_door);		
		removeRecipe(Items.dark_oak_door);		
		/* Remove Wooden Fences */
		removeRecipe(Item.getItemFromBlock(Blocks.oak_fence));
		removeRecipe(Item.getItemFromBlock(Blocks.spruce_fence));
		removeRecipe(Item.getItemFromBlock(Blocks.birch_fence));
		removeRecipe(Item.getItemFromBlock(Blocks.jungle_fence));
		removeRecipe(Item.getItemFromBlock(Blocks.acacia_fence));		
		removeRecipe(Item.getItemFromBlock(Blocks.dark_oak_fence));
		/* Remove Wooden Fence Gates */
		removeRecipe(Item.getItemFromBlock(Blocks.oak_fence_gate));
		removeRecipe(Item.getItemFromBlock(Blocks.spruce_fence_gate));
		removeRecipe(Item.getItemFromBlock(Blocks.birch_fence_gate));
		removeRecipe(Item.getItemFromBlock(Blocks.jungle_fence_gate));
		removeRecipe(Item.getItemFromBlock(Blocks.acacia_fence_gate));		
		removeRecipe(Item.getItemFromBlock(Blocks.dark_oak_fence_gate));		
		/* Remove Chest */
		removeRecipe(Item.getItemFromBlock(Blocks.chest));
		/* Remove Ingots */
		removeRecipe(Items.gold_ingot);
		removeRecipe(Items.iron_ingot);
		/* Remove Torch */
		removeRecipe(Item.getItemFromBlock(Blocks.torch));
		/* Remove Armor */
		removeRecipe(Items.diamond_helmet);
		removeRecipe(Items.diamond_chestplate);
		removeRecipe(Items.diamond_leggings);
		removeRecipe(Items.diamond_chestplate);
		removeRecipe(Items.diamond_boots);
		removeRecipe(Items.golden_helmet);
		removeRecipe(Items.golden_chestplate);
		removeRecipe(Items.golden_leggings);
		removeRecipe(Items.golden_chestplate);
		removeRecipe(Items.golden_boots);
		removeRecipe(Items.leather_helmet);
		removeRecipe(Items.leather_chestplate);
		removeRecipe(Items.leather_leggings);
		removeRecipe(Items.leather_chestplate);
		removeRecipe(Items.leather_boots);
		removeRecipe(Items.iron_helmet);
		removeRecipe(Items.iron_chestplate);
		removeRecipe(Items.iron_leggings);
		removeRecipe(Items.iron_chestplate);
		removeRecipe(Items.iron_boots);
		/* Remove Tools */
		removeRecipe(Items.diamond_pickaxe);
		removeRecipe(Items.diamond_axe);
		removeRecipe(Items.diamond_hoe);
		removeRecipe(Items.diamond_shovel);
		removeRecipe(Items.diamond_sword);
		removeRecipe(Items.golden_pickaxe);
		removeRecipe(Items.golden_axe);
		removeRecipe(Items.golden_hoe);
		removeRecipe(Items.golden_shovel);
		removeRecipe(Items.golden_sword);
		removeRecipe(Items.iron_pickaxe);
		removeRecipe(Items.iron_axe);
		removeRecipe(Items.iron_hoe);
		removeRecipe(Items.iron_shovel);
		removeRecipe(Items.iron_sword);
		removeRecipe(Items.wooden_pickaxe);
		removeRecipe(Items.wooden_axe);
		removeRecipe(Items.wooden_hoe);
		removeRecipe(Items.wooden_shovel);
		removeRecipe(Items.wooden_sword);
		removeRecipe(Items.stone_pickaxe);
		removeRecipe(Items.stone_axe);
		removeRecipe(Items.stone_hoe);
		removeRecipe(Items.stone_shovel);
		removeRecipe(Items.stone_sword);
		/* Remove Misc. Items */
		removeRecipe(Item.getItemFromBlock(Blocks.ladder));
		removeRecipe(Item.getItemFromBlock(Blocks.trapdoor));
		removeRecipe(Item.getItemFromBlock(Blocks.wooden_pressure_plate));
		removeRecipe(Items.fishing_rod);
	}

	public static void removeRecipe(Item itemToRemove) {
		// Learn how to use an iterator instead.  Supposedly this is better than for loops.
	
		List<IRecipe> remove = new ArrayList();
		for(IRecipe recipe : (List<IRecipe>)CraftingManager.getInstance().getRecipeList())
			if(recipe.getRecipeOutput() != null &&  recipe.getRecipeOutput().getItem() == itemToRemove)
				remove.add(recipe);
		
		remove = Lists.reverse(remove);
		
		for(IRecipe r : remove)
			CraftingManager.getInstance().getRecipeList().remove(r);
	}
	
	public static void removeSmeltingRecipe(Item itemToRemove) {
		 Map smeltingList = FurnaceRecipes.instance().getSmeltingList();
		 Iterator it = smeltingList.entrySet().iterator();
		 while(it.hasNext()){
			 Map.Entry pair = (Map.Entry)it.next();
			 if(((ItemStack)pair.getValue()).getItem() == itemToRemove)
				 it.remove();
		 }
	}
}